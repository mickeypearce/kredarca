'use strict';

/* App Module */


var kredarcaApp = angular.module('kredarcaApp', [
    'kredarcaControllers', 
    'kredarcaFilters',
    'kredarcaServices',
    'ngRoute',
    'ngAnimate',
    'ui.bootstrap',
    'environment',
    'angulartics',
    'angulartics.google.analytics',
    'firebase'
]);


kredarcaApp.config(['$routeProvider', '$locationProvider', '$httpProvider', 'toastrConfig',
function($routeProvider, $locationProvider, $httpProvider, toastrConfig) {
    $httpProvider.interceptors.push('httpInterceptor');      
	
    $routeProvider.
      when('/start', {
        templateUrl: 'partials/start.html',
        controller: 'StartCtrl'
      }).    
      when('/dashboard', {
        templateUrl: 'partials/dashboard.html',
        controller: 'DashboardCtrl'
      }).       
      otherwise({
        redirectTo: '/start'   
      });
    
    // $locationProvider.html5Mode(true);
    // $locationProvider.hashPrefix('!');
    
    angular.extend(toastrConfig, {
      // autoDismiss: true,
      // containerId: 'toast-container',
      maxOpened: 1,    
      // newestOnTop: true,
      positionClass: 'toast-bottom-right',
      // preventDuplicates: false,
      preventOpenDuplicates: true
      // target: 'body'
    });

    
  }]);
  
//kredarcaApp.config(function($mdThemingProvider) {
//  $mdThemingProvider.theme('default')
//    .primaryPalette('pink')
//    .accentPalette('orange');
//});


kredarcaApp.factory('httpInterceptor', function httpInterceptor ($q, $window, $location) {
    return {
      response: function(response) {
        // do something on success        
        return response;
      },
      responseError: function(response) {
        // do something on error        
        if (response.status === 401) {
          $location.url('/login');
	      };
        console.log('Intercepted.');
	      return $q.reject(response);
      }
    };
	});
    
    

angular.module('kredarcaApp').
	config(function(envServiceProvider) {
        		
		envServiceProvider.config({
			domains: {
				development: ['localhost'],
				production: ['kredarca.appspot.com', 'kredarca.si', 'www.kredarca.si']
			},
			vars: {
				development: {
                    // No BackEnd at the moment
                    // server: {
                    //     host: 'http://localhost:8080/_ah/api',
                    //     api: 'kredarca',
                    //     version: 'v1' 
                    // },		
                    // Getting provider api responses from resource files
                    testMode: true,
                    // Enabling CORS - Cross-Origin Resource Sharing through proxy that adds header "Access-Control-Allow-Origin", "*" to response
                    proxy: 'http://localhost:8080/proxy/'                                        			 
				},
				production: {			
                    // server: {
                    //     host: 'http://kredarca.appspot.com/_ah/api',
                    //     api: 'kredarca',
                    //     version: 'v1' 
                    // },		
                    testMode: false,                    
                    // proxy: 'http://kredarca.appspot.com/proxy/'
                    proxy: 'http://www.kredarca.si/proxy/'
                    
				}
			}
		});
 		
		envServiceProvider.check();
	});


  angular.module('kredarcaApp').
    config(function() {

      var config = {        
        apiKey: "AIzaSyAPUQFviQO1kF-5ippIV_K0HEsE0gEJu5k",
        authDomain: "kredarca.firebaseapp.com",
        databaseURL: "https://kredarca.firebaseio.com",
        storageBucket: "",
        messagingSenderId: "186137156715"
      };

      firebase.initializeApp(config);
    });

