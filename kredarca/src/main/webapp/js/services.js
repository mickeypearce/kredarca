'use strict';

/* Services */

angular.module('kredarcaServices', []);


angular
    .module('kredarcaServices')
    .factory('brUtils', brUtils);
	    
brUtils.$inject = ['$http', '$q', 'envService'];
function brUtils($http, $q, envService) {
    	
    var ENV = envService.read('all');
	
	var mapByKey = function(data, key, content){	
		key = key || 'id';
		var map = {};
		angular.forEach(data, function(value, index){
			if (!content) {
				map[value[key]] = value;
			} else {
				map[value[key]] = value[content];	
			};
    	});
		return map;
	};
	
	var codeListFromObject = function(obj) {
		var codeList = [];
		Object.keys(obj).forEach(function(key) {
			codeList.push({
				"id": key
			,	"name": obj[key]	
			});
		});		
		return codeList;	
	};
    
    /**
     * Build Enums from array
     * @param {string[]} array Values to enumerate, ex: ['value1', 'value2']
     * @returns {object} Enumarated object, ex: {value1: 'value1', value2: 'value2'}
     */
    var enumerate = function(array) {
        var enums = {};
        angular.forEach(array, function(value){
            enums[value] = value;    
        });  
        return enums;  
    };	
	    
	var getErrorAsync = function(error){
		// var p = $q.defer();				
		// p.resolve({"data": error});
		// return p.promise;	
        return $q(function(resolve){
            resolve({"data": error});    
        });
	}
	
	/** 
     * Parse formatted number string to number 
     * ex. '2.234,5678' to 2234.5678
     * ! Default return = 0
     * ! It parses Empty arg or Alphabetical arg (no digits) or NaN to 0
     */
	var parseNumber = function(num) {	
        //0 is better than NaN if empty	
		num = num || 0;
        //Replace everything but numbers and ',' to '', then replace , to .
        var numParsed = angular.isString(num) ? parseFloat(num.replace(/[^0-9-,]/g, '').replace(',', '.')) : num;                        
		return !isNaN(numParsed) ? numParsed : 0; 
	};
	
	//Parse a string of key-value pairs into an object
	// _.object(_.map('JSESSIONID=5A4A2106CCFC0CA3F24A44DCA64E1687; Path=/izracuni'.replace(/ /g, "").split(';'), function(num) {return num.split('='); })).JSESSIONID;
	var objectify = function(string, delimiter, separator) {
		delimiter = delimiter || '=';
		separator = separator || ';'
		return _.object(_.map(string.replace(/\s/g, '').split(separator), function(num) { return num.split(delimiter); }));	
	};
	
    var loans = [       
        /** @TODO
         * 1. apiPath => apiParams: {urlPath: 'osebni'}, {productId: {shortterm: 12, longterm: 13}}, ...
         * 2. type => properties: {loanType: 'FAST'}, {loanType: 'CONS', aprType: 'FIX'}
         */                        
        {providerId: 'NLB', type: 'CONS', name: 'NLB Osebni kredit', link: 'http://www.nlb.si/osebni-kredit', apiPath: 'osebni'}
    ,   {providerId: 'NLB', type: 'AUTO', name: 'NLB Avtokredit', link: 'http://www.nlb.si/avtokredit', apiPath: 'avto'}
    ,   {providerId: 'NLB', type: 'HOME', name: 'NLB Stanovanjski kredit', link: 'http://www.nlb.si/stanovanjski-kredit', apiPath: 'stanovanjski'}  
    ,   {providerId: 'SBB', type: 'FAST', name: 'Hitri kredit', link: 'https://www.sberbank.si/osebne-finance/krediti/hitri-kredit', apiPath: '3', purposeLoanType: 'CSH'}
    ,   {providerId: 'SBB', type: 'CONS', name: 'Gotovinski kredit', link: 'https://www.sberbank.si/osebne-finance/krediti/gotovinski-kredit', apiPath: '4', purposeLoanType: 'CSH'}
    ,   {providerId: 'SBB', type: 'CONS', name: 'Potrošniški krediti', link: 'https://www.sberbank.si/osebne-finance/krediti/potrosniski-kredit', apiPath: '4', purposeLoanType: 'PRP'}    
    ,   {providerId: 'SBB', type: 'HOME', name: 'Stanovanjski kredit', link: 'https://www.sberbank.si/osebne-finance/krediti/stanovanjski-kredit', apiPath: '2', purposeLoanType: 'PRP'}
    ,   {providerId: 'SKB', type: 'FAST', name: 'Hitri gotovinski kredit', link: 'http://www.skb.si/osebne-finance/krediti/hitri-gotovinski-kredit', apiPath: 'fastcashloan'}
    ,   {providerId: 'SKB', type: 'CONS', name: 'Gotovinski kredit', link: 'http://www.skb.si/osebne-finance/krediti/gotovinski-kredit', apiPath: 'cashloan'}
    ,   {providerId: 'SKB', type: 'HOME', name: 'Stanovanjski kredit', link: 'http://www.skb.si/osebne-finance/krediti/stanovanjski-kredit', apiPath: 'mortgageloan'}
    ,   {providerId: 'ABA', type: 'CONS', name: 'Potrošniški kredit', link: 'http://www.abanka.si/krediti/potrosniski-kredit', apiPath: '0'}
    ,   {providerId: 'ABA', type: 'HOME', name: 'Stanovanjski kredit', link: 'http://www.abanka.si/krediti/stanovanjski-kredit', apiPath: '1'}     
    ,   {providerId: 'NKB', type: 'FAST', name: 'Kredit Takoj', link: 'http://www.nkbm.si/kredit-takoj', apiPath: '12,13'}
    ,   {providerId: 'NKB', type: 'CONS', name: 'Kredit Klasik', link: 'http://www.nkbm.si/kredit-klasik', apiPath: '5,6'}
    ,   {providerId: 'NKB', type: 'HOME', name: 'Stanovanjski kredit', link: 'http://www.nkbm.si/stanovanjski-kredit', apiPath: '14,14'}    
    ,   {providerId: 'UNI', type: 'CONS', name: 'Potrošniški kredit', link: 'https://www.unicreditbank.si/si/pi/borrowing/consumer-loan.html', apiPath: 'consumer-loan/_jcr_content/ucr-header-parsys/sm_form_740e.html', serviceName: 'personal-loan'}
    ,   {providerId: 'UNI', type: 'HOME', name: 'Stanovanjski kredit', link: 'https://www.unicreditbank.si/si/pi/borrowing/Mortgage-loan.html', apiPath: 'Mortgage-loan/_jcr_content/ucr-header-parsys/sm_form.html', serviceName: 'mortgage'}    
    ,   {providerId: 'BKP', type: 'FAST', name: 'Mini kredit', link: 'http://www.banka-koper.si/Fizicne_osebe/Krediti_in_lizing/Potrosniski_krediti/Mini_kredit', apiPath: 'Krediti_in_lizing/Potrosniski_krediti/mini_kredit'}   
    ,   {providerId: 'BKP', type: 'CONS', name: 'Gotovinski kredit', link: 'http://www.banka-koper.si/Fizicne_osebe/Krediti_in_lizing/Potrosniski_krediti/Gotovinski_kredit', apiPath: 'Krediti_in_lizing/Potrosniski_krediti/gotovinski_kredit'}
    ,   {providerId: 'BKP', type: 'HOME', name: 'Stanovanjski kredit', link: 'http://www.banka-koper.si/Fizicne_osebe/Krediti/Stanovanjski_kredit/Stanovanjski_kredit', apiPath: 'Krediti/Stanovanjski_kredit/stanovanjski_kredit'}
    ,   {providerId: 'LON', type: 'FAST', name: 'Hitri kredit', link: '', apiPath: ''}
    ,   {providerId: 'LON', type: 'CONS', name: 'Osebni kredit', link: '', apiPath: ''}
    ];
    var providers = [
        {id: 'NLB', name: 'NLB', longName: 'NLB d.d.', apiUrl: 'http://www.nlb.si/ajax/krediti-izracun', link: 'http://www.nlb.si'}
    ,   {id: 'SBB', name: 'SBERBANK', longName: 'Sberbank banka d.d.', apiUrl: 'https://www.sberbank.si/scredits/', link: 'https://www.sberbank.si'}    
    ,   {id: 'SKB', name: 'SKB', longName: 'SKB banka d.d.', apiUrl: 'http://www.skb.si/izracuni/loan/', link: 'http://www.skb.si'}
    ,   {id: 'ABA', name: 'ABANKA', longName: 'Abanka d.d', apiUrl: 'http://www.abanka.si/.rest/abp/informative-calculations/v1/credits/calculate', link: 'http://www.abanka.si'}
    ,   {id: 'NKB', name: 'NKBM', longName: 'Nova KBM d.d.', apiUrl: 'https://www.nkbm.si/ajaxproductcalculator.ashx', link: 'http://www.nkbm.si'}
    ,   {id: 'UNI', name: 'UNICREDIT', longName: 'UniCredit Banka Slovenija d.d.', apiUrl: 'https://www.unicreditbank.si/si/pi/borrowing/', link: 'http://www.unicreditbank.si'}
    ,   {id: 'BKP', name: 'BANKA KOPER', longName: 'Banka Koper d.d.', apiUrl: 'http://banka-koper.si/Fizicne_osebe/', link: 'http://banka-koper.si'}
    // ,   {id: 'LON', name: 'HRANILNICA LON', longName: 'Hranilnica LON d.d.', apiUrl: 'http://www.lon.si/LON_SI.asp', link: 'http://www.lon.si'}
    ];
    
    /** 
     * @description
     * Get laons data
     * 
     * @param {string} ID of provider
     * @param {string} Loan type - loanType enum 
     * @returns {Array} Collection of loans
     */
    var getLoans = function(filter) {
        if (angular.isUndefined(filter))
            return loans;
        else
            return _.where(loans, filter);
    };    
    
        
    
    /** 
     * @description
     * Get providers data
     * 
     * @param {string} ID of provider, if undefined returns all providers
     * @returns {Array} Collection of providers
     */
    var getProviders = function(providerId) {
        if (angular.isUndefined(providerId))
            return providers;
        else
            return _.where(providers, {id: providerId});
    };
    
    /**
     * @description
     * ALL: Only membership quotes (relationType = MEM)
     * NON: Only non-membership quotes (relationType = NON)
     * ANY: Both quotes - (relationType in (MEM, NON))
     * SOM: Mem quotes for selected providers (loan.memberships), non for others
     */
    var membershipTypeList = [
        {id: 'ANY', name: 'Ni pomembno'}
    ,   {id: 'ALL', name: 'Komitenti'}
    ,   {id: 'NON', name: 'Nekomitenti'}    
    ,   {id: 'SOM', name: 'Izbrano', hidden: true}            
    ];
    var loanTypeList = [
        {id: 'FAST'}
    ,   {id: 'CONS'}
    ,   {id: 'AUTO'}
    ,   {id: 'HOME'}          
    ];
    var aprTypeList = [
        {id: 'FIX', name: 'Fiksna'}
    ,   {id: 'VAR', name: 'Spremenljiva'}            
    ];
    var purposeTypeList = [
        {id: 'CASH', name: 'Gotovina', description: 'Gotovinsko izplačilo na vaš transakcijski račun'}
    ,   {id: 'PURP', name: 'Nakup', description: 'Plačilo na račun prodajalca po predračunu za plačilo nakupa izdelkov/storitev'}
    ,   {id: 'HOME', name: 'Stanovanje', hidden: false, description: 'Nakup stanovanja ali hiše'}
    ,   {id: 'AUTO', name: 'Avto (Kmalu)', hidden: true}                
    ];
    var securityTypeList = [
		{id: 'INS', name: 'Zavarovalnica'}
	,	{id: 'PRP', name: 'Hipoteka'}
	,	{id: 'BFF', name: 'Porok'}
    ,	{id: 'OUT', name: 'Brez zavarovanja', hidden: true}
    ,   {id: 'OTH', name: 'Ostalo', hidden: true}          
    ];
    var purposeLoanTypeList = [
		{id: 'CSH', name: 'Gotovinski'}
	,	{id: 'PRP', name: 'Namenski'}
	];	
	var relationTypeList = [
		{id: 'MEM', name: 'Komitent'}
	,	{id: 'NON', name: 'Nekomitent'}
	];   

    
    var membershipTypeEnum = mapByKey(membershipTypeList, 'id', 'id');     
    // Ex. loanTypeEnum = {FAST: 'FAST', CONS: 'CONS'}
    var loanTypeEnum = mapByKey(loanTypeList, 'id', 'id');
    var aprTypeEnum = mapByKey(aprTypeList, 'id', 'id');
    var securityTypeEnum = mapByKey(securityTypeList, 'id', 'id');
	var purposeTypeEnum = mapByKey(purposeTypeList, 'id', 'id');
	var purposeLoanTypeEnum = mapByKey(purposeLoanTypeList, 'id', 'id'); 
	var relationTypeEnum = mapByKey(relationTypeList, 'id', 'id'); 
    
    // var providersEnum = enumerate(
	// 	// ['NLB', 'SBB', 'LON', 'SKB', 'ABA', 'NKB', 'UNI', 'BKP']      
    //     _.map(getProviders(), function(provider){ return provider.id; })
	// );
    var providersEnum = mapByKey(getProviders(), 'id', 'id');
    
    /* Maps */
    var aprTypeMap = mapByKey(aprTypeList);
    var securityTypeMap = mapByKey(securityTypeList);
    var purposeLoanTypeMap = mapByKey(purposeLoanTypeList);
    var relationTypeMap = mapByKey(relationTypeList);
    var purposeTypeMap = mapByKey(purposeTypeList);
    

  	var getResource = function(provider, loan){
        return 'resources/'+ provider.id + '-' + loan.loanType + '-' + loan.securityType + '-' + loan.aprType;      
    }
    
	return {
		ENV: ENV
	,	trans: {codeListFromObject: codeListFromObject, mapByKey: mapByKey, parseNumber: parseNumber, objectify: objectify}
	,	enums: {aprType: aprTypeEnum, securityType: securityTypeEnum, purposeType: purposeTypeEnum, relationType: relationTypeEnum, loanType: loanTypeEnum, 
                purposeLoanType: purposeLoanTypeEnum, membershipType: membershipTypeEnum, providers: providersEnum}
	,	errors: {getErrorAsync: getErrorAsync}
    ,   lists: {aprType: aprTypeList, purposeType: purposeTypeList, securityType: securityTypeList, relationType: relationTypeList,
                purposeLoanType: purposeLoanTypeList, membershipType: membershipTypeList}
    ,   maps: {aprType: aprTypeMap, securityType: securityTypeMap, purposeLoanType: purposeLoanTypeMap, relationType: relationTypeMap,
               purposeType: purposeTypeMap}
    /* returns Array of objects*/ 
    ,	data: {getProviders: getProviders, getLoans: getLoans}  
    ,   test: {getResource: getResource}  
	};
};


angular
    .module('kredarcaServices')
    .factory('QuotesService', QuotesService);
	
QuotesService.$inject = ['$q', 'brUtils', 'SberbankApi', 'NlbApi', 'LonApi', 'SkbApi', 'AbankaApi', 'NkbmApi', 'UnicreditApi', 'BankaKoperApi'];
function QuotesService($q, brUtils, SberbankApi, NlbApi, LonApi, SkbApi, AbankaApi, NkbmApi, UnicreditApi, BankaKoperApi) {
		
	var purposeType = brUtils.enums.purposeType,
        purposeLoanType = brUtils.enums.purposeLoanType,
		aprType = brUtils.enums.aprType,
		relationType = brUtils.enums.relationType,
		loanType = brUtils.enums.loanType,        
        apis = [
        SberbankApi.getLoan
        ,   
        NlbApi.getLoan
        ,   
        SkbApi.getLoan                        
        ,   
        AbankaApi.getLoan        
        ,   
        NkbmApi.getLoan
        ,   
        UnicreditApi.getLoan
        ,   
        BankaKoperApi.getLoan
        // ,   LonApi.getLoan
        ];
	
	var getQuotes = function(loan) {
		// return brGapi.execute("quotes.get", loan);		
		var p = $q.defer();		
		
		var calls = []
        ,   params = []
        /* Permutations of parameters for Api calls, ex.:
           AbankaApi.getLoan({loanType: loanType.FAST, amount: loan.amount, duration: loan.duration, purposeLoan: purposeLoanType.CSH, aprType: aprType.FIX, securityType: loan.securityType, relationType: relationType.MEM})
           Params are split into 3. group-types based on their nature and then joined back together with extend function in actual api call:
           AbankaApi.getLoan(angular.extend({}, base, fast_cash, fix_mem)) 
        */
        // 1.part - static loan variables
        ,   base = {amount: loan.amount, duration: loan.duration, securityType: loan.securityType}
        // 2.part - loan and purpose types permutations
        ,   fast_cash = {loanType: loanType.FAST, purposeLoan: purposeLoanType.CSH} /* Fast is only cash*/ 
        ,   cons_cash = {loanType: loanType.CONS, purposeLoan: purposeLoanType.CSH}
        ,   cons_purp = {loanType: loanType.CONS, purposeLoan: purposeLoanType.PRP}
        ,   home = {loanType: loanType.HOME, purposeLoan: purposeLoanType.PRP}
        // 3.part - apr and relation types permutations
        ,   fix_mem = {aprType: aprType.FIX, relationType: relationType.MEM}
        ,   fix_non = {aprType: aprType.FIX, relationType: relationType.NON}
        ,   var_mem = {aprType: aprType.VAR, relationType: relationType.MEM}
        ,   var_non = {aprType: aprType.VAR, relationType: relationType.NON}
        
        ;
        
		if (loan.purpose === purposeType.CASH){            
            params = [
                angular.extend({}, base, fast_cash, fix_mem)
            ,	angular.extend({}, base, fast_cash, var_mem)
            ,	angular.extend({}, base, cons_cash, fix_mem)
            ,	angular.extend({}, base, cons_cash, fix_non)
            ,	angular.extend({}, base, cons_cash, var_mem)
            ,	angular.extend({}, base, cons_cash, var_non)
            ];            
		} else 
		if (loan.purpose === purposeType.PURP){            
            params = [
                angular.extend({}, base, cons_purp, fix_mem)
            ,	angular.extend({}, base, cons_purp, fix_non)
            ,	angular.extend({}, base, cons_purp, var_mem)
            ,	angular.extend({}, base, cons_purp, var_non)
            ];       
        } else 
		if (loan.purpose === purposeType.HOME){            
            params = [
                angular.extend({}, base, home, fix_mem)
            ,	angular.extend({}, base, home, fix_non)
            ,	angular.extend({}, base, home, var_mem)
            ,	angular.extend({}, base, home, var_non)
            ];      				
		} else {
			
		};

        // We defined parameters, lets make an array of api calls, ex.result:		
        // calls = [
        //      SberbankApi.getConsumer({amount: loan.amount, duration: loan.duration, purpose: brUtils.enums.purposeType.CASH, aprType: brUtils.enums.aprType.FIX, securityType: loan.securityType, relationType: brUtils.enums.relationType.MEM})
        // ,	AbankaApi.getLoan(angular.extend({}, base, cons_cash, var_non)) 
        // ,	brGapi.execute("quote1.get", loan)       
        // ]	
        apis.forEach(function(api){
            params.forEach(function(param){
                calls.push(api(param)); //calls.push(api.call(this, param));       
            });                    
        });	
		
        // Lets execute those calls and collect results
		var pending = calls.length,
			rejected = 0,
			quotes = [],
			res = {"data": {"loan": loan, "quotes": []}};
		
		calls.forEach(function(call, index){
			call.then(function(resp) {					
				if(!resp.data.code){                       
					//Add response of a call to array of results (quotes)
					quotes.push(resp.data);
				} else {
                    // Defined errors
                    // console.info (code < 5000): No queries exceptions (before api call)
                    // console.warn: Response parsing exceptions (after api call)                    
                    console[resp.data.code < 5000 ? 'info' : 'warn'](resp.data.provider.id + ' (' + resp.data.code + '): ' + resp.data.error + ', query=' + angular.toJson(resp.data.query));					
				};							
			}).catch(function(error){
				console.error('Error: ' + angular.toJson(error));
				//Reject only if all calls fail
				if (calls.length === ++rejected){
					p.reject(error);	
				};
			}).then(function(){		
                --pending;		
                //Notify progress                
                p.notify({
                    current: (calls.length - pending),
                    total: calls.length
                });
                //All calls finished, return result                
				if (0 === pending){					
					res.data.quotes = quotes;
					p.resolve(res);	
				};	                			
			});	
				
		});
		
		/*
		//$q.all quits with rejection if one promise (call) fails
		$q.all([
			brGapi.execute("quote1.get", loan)
		,	brGapi.execute("quote2.get", loan)	
		]).then(function(quotes){
			var _quotes = quotes.map(function(quote){
				return quote.data;
			});
			var res = {"data": {"quotes": _quotes}};
			console.log('res=' + angular.toJson(res));
			p.resolve(res);	
		});
		*/
		return p.promise;		
	};
		
	return {
		getQuotes: getQuotes
	};
		
}; 


(function() {
'use strict';

    angular
        .module('kredarcaServices')
        .service('MailService', MailService);

    MailService.$inject = ['brUtils', '$http'];
    function MailService(brUtils, $http) {
        var apiKey = '0d80dec8dae1ff326b24da7f173a84adc2b243cb';
        var apiUrl = 'https://api.sparkpost.com/api/v1/transmissions';
        
        this.sendAttachment = sendAttachment;
        this.send = send;
        
        ////////////////        
        function sendAttachment(toAddress, attachmentData) {
            return $http({
                url: brUtils.ENV.proxy + apiUrl
            ,   method: 'POST'            
            ,   headers: {
			        'Content-Type': 'application/json',
                    'Authorization': apiKey
				}   
            ,   data: {
                    "campaign_id": "pdf",    
                    "recipients": [
                        {
                            "address": {
                                "email": "info@kredarca.si",
                                "header_to": toAddress
                            }          
                        }
                    ],
                    "content": {
                        // "template_id": "kredarca-pdf",  //Use stored template, but no attachment                        
                        "from": {
                            "name": "Kredarca",
                            "email": "info@kredarca.si"
                        },
                        "subject": "Primerjava ponudb kreditov",
                        "text": "Pozdravljeni. Pošiljam primerjavo kreditov, kot smo obljubili. Na svidenje.",
                        "html": "<strong>Pozdravljeni</strong><p>Pošiljamo primerjavo kreditov, kot smo obljubili.</p><p>Nasvidenje!</p>",
                        "attachments": [
                            {
                                "type": "application/pdf",
                                "name": "krediti.pdf",
                                "data": attachmentData
                            }
                        ]
                    }
                
                } 
            }).then(function(result){
                return {
                    "success": (result.data.results.total_accepted_recipients == 1)
                };                 
            });
        };

        function send(text) {
            return $http({
                url: brUtils.ENV.proxy + apiUrl
            ,   method: 'POST'            
            ,   headers: {
			        'Content-Type': 'application/json',
                    'Authorization': apiKey
				}   
            ,   data: {
                    "campaign_id": "contact",    
                    "recipients": [
                        {
                            "address": {
                                "email": "info@kredarca.si"
                            }          
                        }
                    ],
                    "content": {                                               
                        "from": {
                            "name": "Kredarca",
                            "email": "info@kredarca.si"
                        },
                        "subject": "Sporočam",
                        "text": text
                    }
                
                } 
            }).then(function(result){
                return {
                    "success": (result.data.results.total_accepted_recipients == 1)
                };                 
            });
        };        
    }
    
})();



