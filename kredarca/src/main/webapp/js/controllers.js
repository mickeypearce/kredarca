(function() {
    'use strict';

    /* Controllers */    

    angular.module('kredarcaControllers', [
        'ngProgress', 
        'angular-steps', 
        'ui.select', 
        'ngSanitize',
        'toastr'           
    ]);
    
    angular
        .module('kredarcaControllers')
        .controller('StartCtrl', StartCtrl);
        
    StartCtrl.$inject = ['$scope', '$window', 'QuotesService', '$uibModal', 'brUtils', '$timeout', 'ngProgressFactory', 'MailService', 
        'toastr', '$analytics', '$firebaseArray', '$firebaseObject', '$routeParams'];
    function StartCtrl($scope, $window, QuotesService, $uibModal, brUtils, $timeout, ngProgressFactory, MailService, 
        toastr, $analytics, $firebaseArray, $firebaseObject, $routeParams) {
                    
        $scope.getQuotes = function(loan) {		    
            
            $scope.loading = true; 
            $scope.progress = {current: 2, total: 100};                            
            
            QuotesService.getQuotes(loan).then(function(resp) {
                
                angular.forEach(resp.data.quotes, function(quote){                
                    // Mapping query codes to names  
                    quote.securityTypeName = $scope.maps.securityType[quote.query.securityType].name;                     
                    quote.aprTypeName = $scope.maps.aprType[quote.query.aprType].name; 
                    quote.relationTypeName = $scope.maps.relationType[quote.query.relationType].name;
                    quote.purposeLoanTypeName = $scope.maps.purposeLoanType[quote.query.purposeLoan].name; 
                });
                
                $scope.quotes = resp.data.quotes;	            			
                // console.log('$scope.quotes=' + angular.toJson($scope.quotes));
                // console.log('result=' + angular.toJson(resp.data));		
            
            // Catch    
            }, function(error) {
                //Notification of some kind @TODO                
                console.error('Error=' + angular.toJson(error.data));
                $scope.quotes = [];
            // Notify                               
            }, function(progress){                                       
                $scope.progress = progress;           
            // Finally             
            }).then(function(){                
                // 1sec to complete progress bar then hide           
                $timeout(function(){
                    $scope.loading = false;    
                }, 1000);
                
            });
            
        };
                        
        $scope.filterMembership = function(quote) {
            // If I'm a member of a provider, filter out non-members quotes for that provider
            // $scope.memberships with all providers it's the same as filtering with filters.query.relationType = 'MEM'
            // ($scope.memberships = [] it's the same as filters.query.relationType = 'NON' 	
            // membershipType == Any: show all quotes regarding membership
            var isMember = ($scope.loan.memberships.indexOf(quote.provider.id) !== -1)
            ,	isQuoteForMembers = (quote.query.relationType === $scope.enums.relationType.MEM);
            return ($scope.loan.membershipType === $scope.enums.membershipType.ANY) || (isMember && isQuoteForMembers) || (!isMember && !isQuoteForMembers);
        };
        
        $scope.setMemberships = function() {
            if ($scope.loan.membershipType == $scope.enums.membershipType.ALL) {
                // Array of IDs of all providers (membershipType.ALL)
                $scope.loan.memberships = Object.keys($scope.enums.providers); //_.pluck($scope.providers, 'id')    
            } else {
                // None (membershipType.NON)
                $scope.loan.memberships = [];    
            };
            
        };
        
        $scope.setMembershipType = function(){
            if ($scope.loan.memberships.length === Object.keys($scope.enums.providers).length) { 
                $scope.loan.membershipType = $scope.enums.membershipType.ALL; 
            } else
            if ($scope.loan.memberships.length === 0) { 
                $scope.loan.membershipType = $scope.enums.membershipType.NON; 
            } else {
                $scope.loan.membershipType = $scope.enums.membershipType.SOM;   
            };     
        };
        
        $scope.gotoStep = function (name) {
            $scope.currentStep = name;
        };
        
        $scope.isCurrentStep = function (name) {        
            return angular.isArray(name) ? (name.indexOf($scope.currentStep) >= 0) : (name === $scope.currentStep);    
        };       

        $scope.$watch("currentStep", function(newValue, oldValue){
            if (newValue === oldValue) { return; };
            switch(newValue) {                
                case "k-guide-step-4":
                    // Reset compares
                    $scope.resetCompares();
                    // Add recommended quote for comparing
                    $scope.filtered[0].compare = true;
                    $scope.filtered[1].compare = true;
                    break;
                case "k-filter-step-0":  
                    // Reset filters                  
                    $scope.resetFilters();
                    // Reset compares
                    $scope.resetCompares();
                    break;                    
            }

        });

        $scope.resetFilters = function () {            
            $scope.loan.membershipType = $scope.enums.membershipType.ANY;
            $scope.filters.provider.id = '!x';
            $scope.filters.query.aprType = '!x';
        };

        $scope.resetCompares = function () {
            $scope.quotes.forEach(function(quote){
                quote.compare = false;
            });
        };
        
        // Catches filtering and sorting changes to "filtered" set
        $scope.$watchCollection("filtered", function(newValue, oldValue){
            if (newValue === oldValue) { return; };
            $scope.updating = true;
            $timeout(function(){
                $scope.updating = false;    
            }, 500);        
        });
        
		// Update filteredComparing every time eval exp changes		
        $scope.$watch(function() {            
            $scope.filteredComparing = $scope.$eval("quotes | filter: {compare: true}");            
        });
		
        $scope.$watchCollection("filteredComparing", function(newValue, oldValue){  
            if (newValue === oldValue) { return; };         
            // We need two quotes to compare 
            if (newValue.length >= 2) {                 
                toastr.success(
                    // 'Izbrane so ' + newValue.length + ' ponudbe od ' + $scope.itemsPerPage + '.', 
                    'Primerjaj izbrane ponudbe', {
                        tapToDismiss: false,    
                        timeOut: 0,
                        extendedTimeOut: 0,
                        onTap: function() {
                            $scope.openCompareModal();    
                    }
                });            
            } else {
                toastr.clear();
            }        
        });        

        $scope.isDisabledCompareCheckbox = function (quote){
            return (($scope.filteredComparing.length > 3) && (quote.compare != true));
        }        
                
        $scope.openCompareModal = function() {
            $analytics.eventTrack('Primerjaj', {  category: 'Commands', label: 'open' });
            var modal = $uibModal.open({
                templateUrl: 'partials/compare.html',
                controller: ['$uibModalInstance', function ($uibModalInstance) {
                    var vm = this;                        
                    vm.close = function() {             
                        $uibModalInstance.close();
                    }
                }],
                controllerAs: 'ctrl',
                scope: $scope,
                size: 'fs',
                animation: false                
            });		
        };

        $scope.openContactModal = function() {
            var modal = $uibModal.open({
                templateUrl: 'partials/contact.html',
                controller: ['$uibModalInstance', function ($uibModalInstance) {
                    var vm = this;                        
                    vm.close = function() {             
                        $uibModalInstance.close();
                    }                    
                }],
                controllerAs: 'ctrl',
                scope: $scope,
                size: 'fs',
                animation: false                
            });		
        };
        
        $scope.exportPdf = function() {            
            var window = $window.open('') //, "width=800, height=800");
            var table = $('#compareModal .modal-body');
            // Scroll to the top of exporting table as html2canvas renders only visible part of element
            $(".modal").scrollTop(table.position().top);
            // Create PDF object     
            var pdf = new jsPDF('landscape', 'pt', 'a4');            
            pdf.addHTML(table, function() {                  
                // var newWindow = pdf.output('dataurlnewwindow');
                window.document.location.href = pdf.output('datauristring');                        
                pdf.save('Krediti_primerjava.pdf');                    
            });        
                        
        }
        
        $scope.sendPdfTo = function(email) {        
            var table = $('#compareModal .modal-body');
            // Scroll to the top of exporting table as html2canvas renders only visible part of element
            $(".modal").scrollTop(table.position().top);
            // Create PDF object                    
            var pdf = new jsPDF('landscape', 'pt', 'a4');    
            $scope.progressbar.start();
            // Create Pdf from html element        
            pdf.addHTML(table, function() { 
                // Get Pdf Data
                var datauri = pdf.output('dataurlstring');   
                // Remove data header
                var data = datauri.replace('data:application/pdf;base64,', '');    
                // Send Pdf to email as an attachment         
                return MailService.sendAttachment(email, data).then(function(result){                    
                    if (result.success) {   
                        $scope.isMailSent = true;                           
                        $timeout(function() { $scope.isMailSent = false; }, 5000);
                    } else {
                        $scope.isMailSentError = true;
                        $timeout(function() { $scope.isMailSentError = false; }, 5000);
                    }                    
                }).catch(function(error){
                    console.error(error);
                    $scope.isMailSentError = true;
                    $timeout(function() { $scope.isMailSentError = false; }, 5000);
                }).then(function(){
                    $scope.progressbar.complete();
                });
                                    
            });        
                        
        };

        $scope.sendContact = function(text) {        
                                   
            $scope.progressbar.start();
                        
            return MailService.send(text).then(function(result){                    
                if (result.success) {   
                    $scope.isMailSent = true;                           
                    $timeout(function() { $scope.isMailSent = false; }, 5000);                                
                } else {
                    $scope.isMailSentError = true;
                    $timeout(function() { $scope.isMailSentError = false; }, 5000);
                }                    
            }).catch(function(error){
                console.error(error);
                $scope.isMailSentError = true;
                $timeout(function() { $scope.isMailSentError = false; }, 5000);
            }).then(function(){
                $scope.progressbar.complete();
            });                                                                    
                        
        };   

        $scope.addEmailToList = function(email) {
            var ref = firebase.database().ref('emails');
            var list = $firebaseArray(ref);
                        
            $scope.progressbar.start();
            list.$add(email).then(function(){                
                $scope.isMailSent = true;                           
                $timeout(function() { $scope.isMailSent = false; }, 9000); 
            }).catch(function(error){
                console.error(error);
                $scope.isMailSentError = true;
                $timeout(function() { $scope.isMailSentError = false; }, 9000);            
            }).then(function(){
                $scope.progressbar.complete();
            });
        };     
        
    
        /* Enumerators, ex.{id1: 'id1'} */
        $scope.enums = brUtils.enums;
        
        /* Lists, ex.[id: 'id1', name: 'name1'] */
        $scope.lists = brUtils.lists;
        $scope.providers = brUtils.data.getProviders();
        
        /* Codelist maps, ex.{id1: name1} */
        $scope.maps = brUtils.maps;        
        
        /* Filtering */ 		
        $scope.filters = {};  
        /* Sorting */
        $scope.sort = 'ear';
        /* Quotes collection */ 
        $scope.quotes = [];
        /* Pagination */
        $scope.currentPage = 1;
        $scope.itemsPerPage = 4;	
        /* View */
        $scope.horizontalView = true;
        /* Steps */ // $scope.gotoStep('k-steps-start');
        $scope.currentStep = 'k-steps-start';
        /* Progress bar */
        $scope.progressbar = ngProgressFactory.createInstance();
        // $scope.progressbar.setParent(document.getElementById('progress-bar'));
        // $scope.progressbar.setAbsolute();
        $scope.progressbar.setColor('#337ab7');
        // $scope.progressbar.setHeight('3px');                
        
        /* Default loan: Values from route params: #/start?amount=2000&duration=12&purpose=HOME&security=PRP
        */   
        $scope.loan = {
            amount: $routeParams.amount || 7500
        , 	duration: $routeParams.duration || 36
        , 	purpose: _.has(brUtils.enums.purposeType, $routeParams.purpose) ? $routeParams.purpose : brUtils.enums.purposeType.CASH
        , 	securityType: _.has(brUtils.enums.securityType, $routeParams.security) ? $routeParams.security : brUtils.enums.securityType.INS
        ,   membershipType: brUtils.enums.membershipType.ANY
        ,   memberships: []
        };
        $scope.getQuotes($scope.loan); 
        
        ////////////                

            
    }

})();


