(function() {
'use strict';

    angular.module('kredarcaServices').factory('NlbApi', NlbApi);
	
	NlbApi.$inject = ['brUtils', '$http'];	
	function NlbApi(brUtils, $http) {
        var provider = brUtils.data.getProviders('NLB')[0];
		var service = {
			getLoan: getLoan
		};
		
		return service;
					
		////////////////
        function getLoan(loan) {
            
            var product = brUtils.data.getLoans({providerId: provider.id, type: loan.loanType})[0];
            
            return get(loan, product).then(function(resp){
                resp.data.query = loan;
                resp.data.provider = provider;                    
                resp.data.product = product;
                return resp;
            });
        }
        
		function get(loan, product) { 			
            
            // No queries
            
            if (loan.loanType === brUtils.enums.loanType.FAST)
				return brUtils.errors.getErrorAsync({
					code: '1040'
				,	error: 'No querys for loanType=' + loan.loanType
				});	 
            
			// Mapping
            
			var p_loan = angular.copy(loan);
			/* z - zavarovalnica, h - hipoteka, b- brez, p - druge oblike */
			p_loan.securityType = (loan.securityType === brUtils.enums.securityType.INS) ? 'z' : 
								(loan.securityType === brUtils.enums.securityType.PRP) ? 'h' : 
								(loan.securityType === brUtils.enums.securityType.OUT) ? 'b' : 'p';
			/* d - denar, a - avto/plovilo, sh - stanovanje/hisa, st - student*/
			p_loan.purpose = (product.type === brUtils.enums.loanType.CONS) ? 'd' :							
                            (product.type === brUtils.enums.loanType.AUTO) ? 'a' :
							(product.type === brUtils.enums.loanType.HOME) ? 'sh' : 'd';
			/* osebni - denar, avto - avto/plovilo, stanovanjski - stanovanje/hisa, studentski - student*/			      
            p_loan.productId = product.apiPath;                            
			/* 1 - komitent, '' - nekomitent*/
			p_loan.relationType = (loan.relationType === brUtils.enums.relationType.MEM) ? '1' : '';
                        
			
			return $http({		   
				url: 
					brUtils.ENV.testMode ? 'resources/'+ 'nlb-potrosnik' +'.json' :
					brUtils.ENV.proxy + 					
                    provider.apiUrl
				, method: 
					brUtils.ENV.testMode ? "GET" :
					"POST"
				, headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
				// ts=1446559346858&v=3000&vu=EUR&a=150&d=24
				// params:{"namen":"d","krediti":["osebni"],"tip":"a","zav":"z","znesek":3001,"anuiteta":150,"rocnost":24,"zz-vita":"","os-rac":"","prej":"","student-26":"","datum_roj":"29.10.1997"}
				, params: { 
					ts: '' //1447335121091
				,	v:	p_loan.amount
				,	vu:	'EUR'
				,	a:	'' //p_loan.annuity
				,	d:	p_loan.duration
				,	params: {
						namen: p_loan.purpose
					,	krediti: [p_loan.productId]
					,	tip: "a"			
					,	zav: p_loan.securityType
                    ,   zav2: 't' //t - triglav, m - maribor / pri purpose = sh (stanovanje)
					,	znesek: p_loan.amount
					,	anuiteta: "" //p_loan.annuity
					,	rocnost: p_loan.duration
					,	"zz-vita": ""
					,	"os-rac": p_loan.relationType
					,	prej: ""  //d-delo, s-stipendija / pri purpose = st (študent)
					,	"student-26": ""
					,	"datum_roj": "29.10.1997" 
					}
				}
				, data: {} 
				, transformResponse: function(data){
					try {
						var json = angular.fromJson(data)
						// Response includes both aprTypes, extracting appropriate
						,	section = _.find(json.data[0].izracuni, function(item){
							return ((loan.aprType === brUtils.enums.aprType.VAR) && (item.tip_om === 'spremenljiva')) || ((loan.aprType === brUtils.enums.aprType.FIX) && (item.tip_om === 'nespremenljiva'));
						});
						
						if (angular.isUndefined(section)) {
							throw 'No querys for aprType=' + loan.aprType;		
						};
						
                        // amountRepay = amount + costsTotal + interestTotal
                        var  
                            costsTotal = section.stroski_zavarovanja + section.stroski_odobritve
                        ,   interestTotal = section.skupni_stroski - costsTotal
                        ,   amountRepay = section.znesek + section.skupni_stroski;
                        
						return {						
						    amount: section.znesek
						,	annuity: section.anuiteta
						,	duration: section.rocnost
						,	apr: section.om
						, 	ear: section.eom
						,	costsTotal: costsTotal
                        ,   interestTotal: interestTotal
						,	amountRepay: amountRepay		
                        ,   aprBase: section.euribor
                        ,   aprMargin: section.pribitek			                        					
						};
					} catch (e) {
						return { code: '5000', error: e };					
					}
				}					
			});	
								
		};
			
	};

})();