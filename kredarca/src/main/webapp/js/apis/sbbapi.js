(function() {
'use strict';
    
    angular.module('kredarcaServices').factory('SberbankApi', SberbankApi);
    
	SberbankApi.$inject = ['brUtils', '$http'];	
	function SberbankApi(brUtils, $http) {
		var provider = brUtils.data.getProviders('SBB')[0];
		var service = {
			getLoan: getLoan
		};
		
		return service;			
        
        ////////////////
        function getLoan(loan) {
            
            var product = brUtils.data.getLoans({providerId: provider.id, type: loan.loanType, purposeLoanType: loan.purposeLoan})[0];
            
            return get(loan, product).then(function(resp){
                resp.data.query = loan;
                resp.data.provider = provider;                    
                resp.data.product = product;
                return resp;
            });
        }	
			
		function get(loan, product) {	            
                                    
            // No queries
                                               
            if (
                // FAST: OUT
                (loan.loanType === brUtils.enums.loanType.FAST && 
                !_.contains([brUtils.enums.securityType.OUT], loan.securityType)) || 
                // CONS, HOME: INS, PRP                
				(_.contains([brUtils.enums.loanType.CONS, brUtils.enums.loanType.HOME], loan.loanType) &&
                !_.contains([brUtils.enums.securityType.INS, brUtils.enums.securityType.PRP], loan.securityType))
            )                                             
                return brUtils.errors.getErrorAsync({
                    code: '1000'
                ,	error: 'No querys for securityType=' + loan.securityType + ' and loanType=' + loan.loanType
                });
                		
			// Mapping
            
			var p_loan = angular.copy(loan);
            /* 1 - fiksna, 2 - spremenljiva*/
			p_loan.aprType = (loan.aprType === brUtils.enums.aprType.FIX) ? '1' : '2';
            /* 1 - zavarovalnica, 2 - hipoteka, '' - brez zavarovanja - Hitri */
			p_loan.securityType = (loan.securityType === brUtils.enums.securityType.OUT) ? '' :
                                  (loan.securityType === brUtils.enums.securityType.INS) ? '1' : 
                                  (loan.securityType === brUtils.enums.securityType.PRP) ? '2' : 'X';
            /* 1 - gotovinski, 2 - namenski */
            p_loan.purposeLoan = (loan.purposeLoan === brUtils.enums.purposeLoanType.CSH) ? '1' : '2';			
			/* 1 - komitent, 4 - nekomitent, (2 - nov komitent) */
			p_loan.relationType = (loan.relationType === brUtils.enums.relationType.MEM) ? '1' : '4';	
            /* 3 - hitri, 4 - potrošniški (oz.gotovinski) */
            p_loan.productId = product.apiPath;	
			
			return $http({		   
				url: 
					brUtils.ENV.testMode ? brUtils.test.getResource(provider, loan) +'.xml':					
					brUtils.ENV.proxy + 					 
                    provider.apiUrl
				, method: 
					brUtils.ENV.testMode ? "GET" :
					"POST"
				, headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
				, params: { command: 'calculate' }
				// , data: "id=4&nacinZavarovanja=1&namenKredita=1&oblikaSodelovanja=1&valuta=1&vrstaOM=1&znesekKredita=1000&odplacilnaDoba=3&zadnjaMesecnaAnuiteta=0&elektronskiNaslov=&dovoliEmail=1&format=html"		
				, data: $.param({
					id: p_loan.productId
				,	nacinZavarovanja: p_loan.securityType
				,	namenKredita: p_loan.purposeLoan
				,	oblikaSodelovanja: p_loan.relationType
				,	valuta: '1'
				,	vrstaOM: p_loan.aprType
				,	znesekKredita: p_loan.amount
				,	odplacilnaDoba: p_loan.duration
				,	zadnjaMesecnaAnuiteta: '0' //'-1'			
				,	elektronskiNaslov: ''
				,	dovoliEmail: '1'
				,	format: 'html'
				})
				, transformResponse: function(data){
					try {
						var 
                            xml = $($.parseXML(data))
						, 	result = xml.find('resultText').text()
						, 	html = $($.parseHTML($.trim(result)));					

                        var 
                            apr = brUtils.trans.parseNumber( html.find( ":contains('Obrestna mera') .pull-right" ).text() )
                        ,   aprMargin = (loan.aprType === brUtils.enums.aprType.VAR) ? 
                                        brUtils.trans.parseNumber( html.find( ":contains('Obrestna mera') .pull-right" ).text().split('+')[1].trim() ) :
                                        apr
                        ,   aprBase = apr - aprMargin
                        // amountRepay = amount + costsTotal + interestTotal
                        ,   amount = brUtils.trans.parseNumber( html.find( ":contains('Skupni znesek kredita') .pull-right" ).text() )                                        
                        ,   amountRepay = brUtils.trans.parseNumber( html.find( ":contains('Skupni znesek, ki ga plača kreditojemalec') .pull-right" ).text() )                        
                        ,   approvalCosts = brUtils.trans.parseNumber( html.find( ":contains('Stroški odobritve') .pull-right" ).text() )
                        ,   securityCosts = brUtils.trans.parseNumber( html.find( ":contains('Stroški zavarovanja') .pull-right" ).text() )                       
                        ,   costsTotal = securityCosts + approvalCosts                         
                        ,   interestTotal = amountRepay - amount - costsTotal;
                        
                        
						return {	
                            amount: amount
						,	annuity: brUtils.trans.parseNumber( html.find( ":contains('Mesečna anuiteta') .pull-right" ).text() )
						,	duration: brUtils.trans.parseNumber(html.find( ":contains('Odplačilna doba') .pull-right" ).text().trim() )
						,	apr: apr 
						, 	ear: brUtils.trans.parseNumber( html.find( ":contains('Efektivna obrestna mera') .pull-right" ).text() )
						,	costsTotal: costsTotal
                        ,   interestTotal: interestTotal
						,	amountRepay: amountRepay 
                        ,   aprBase: aprBase                        
                        ,   aprMargin: aprMargin
						};
					} catch (e) {
						return { code: '5000', error: e };
					}
				}					
			});	
		
		};
			
	};

})();