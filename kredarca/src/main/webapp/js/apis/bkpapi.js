(function() {
'use strict';

	angular.module('kredarcaServices').factory('BankaKoperApi', BankaKoperApi);
	
	BankaKoperApi.$inject = ['brUtils', '$http'];
	function BankaKoperApi(brUtils, $http) {
		var provider = brUtils.data.getProviders('BKP')[0];
		var service = {
			getLoan: getLoan
		};
		
		return service;
					
		////////////////
        function getLoan(loan) {
            
            var product = brUtils.data.getLoans({providerId: provider.id, type: loan.loanType})[0];
            
            return get(loan, product).then(function(resp){
                resp.data.query = loan;
                resp.data.provider = provider;                    
                resp.data.product = product;
                return resp;
            });
        }
        
		function get(loan, product) {		 
			
			// No queries
            
            // MEM
            if (loan.relationType !== brUtils.enums.relationType.MEM)
				return brUtils.errors.getErrorAsync({				
                	code: '1200'
				,	error: 'No querys for relationType=' + loan.relationType
				});
                
            if (
                // INS, PRP, OTH                     
                !_.contains([brUtils.enums.securityType.INS, brUtils.enums.securityType.PRP, brUtils.enums.securityType.OTH], loan.securityType)
            )                                             
                return brUtils.errors.getErrorAsync({
                    code: '1000'
                ,	error: 'No querys for securityType=' + loan.securityType + ' and loanType=' + loan.loanType                    
                });
                                    
            if (
                // FAST: FIX                   
                (loan.loanType === brUtils.enums.loanType.FAST &&                 
                !(loan.aprType === brUtils.enums.aprType.FIX)) ||                 
                // CONS: <=12m: FIX; 12-<=60m: FIX, VAR; >60m: VAR                                 
                (loan.loanType === brUtils.enums.loanType.CONS &&                                                 
                !(loan.aprType === brUtils.enums.aprType.FIX && loan.duration <= 60) && 
                !(loan.aprType === brUtils.enums.aprType.VAR && loan.duration > 12))
            )                                             
                return brUtils.errors.getErrorAsync({
                    code: '1100'
                ,	error: 'No querys for aprType=' + loan.aprType + ' and loanType=' + loan.loanType                    
                });
                
            if ( // Api doesn't error check duration (min - max), we must do it here!
                // FAST: 1-36m                   
                (loan.loanType === brUtils.enums.loanType.FAST && !(loan.duration >= 1 && loan.duration <= 36)) ||                 
                // CONS: 1-120m                                 
                (loan.loanType === brUtils.enums.loanType.CONS && !(loan.duration >= 1 && loan.duration <= 120)) ||
                // HOME: 13-360m                                 
                (loan.loanType === brUtils.enums.loanType.HOME && !(loan.duration >= 13 && loan.duration <= 360))
            )                                             
                return brUtils.errors.getErrorAsync({
                    code: '1100'
                ,	error: 'No querys for duration=' + loan.duration + ' and loanType=' + loan.loanType                    
                });
                
            if ( // Api doesn't error check amount (min - max), we must do it here!
                // FAST: 1-6000               
                (loan.loanType === brUtils.enums.loanType.FAST && !(loan.amount <= 6000)) ||                 
                // CONS: 200-                                 
                (loan.loanType === brUtils.enums.loanType.CONS && !(loan.amount >= 200))
            )                                             
                return brUtils.errors.getErrorAsync({
                    code: '1300'
                ,	error: 'No querys for amount=' + loan.amount + ' and loanType=' + loan.loanType                    
                });
            
                
			// Mapping
                                   
			var p_loan = angular.copy(loan);            
            /* F - fiksna, R - spremenljiva*/
			p_loan.aprType = (loan.aprType === brUtils.enums.aprType.FIX) ? 'F' : 'R';            
            /* 1 - komitent, 0 - nekomitent*/
			/*p_loan.relationType = (loan.relationType === brUtils.enums.relationType.MEM) ? '1' : '0';*/		
			/* 71296 - Zav.Triglav, Hipoteka - hipoteka, Drugo - ostalo */
			p_loan.securityType = (loan.securityType === brUtils.enums.securityType.INS) ? '71296' : 
								(loan.securityType === brUtils.enums.securityType.PRP) ? 'Hipoteka' : 
								(loan.securityType === brUtils.enums.securityType.OTH) ? 'Drugo' : 'X';	                  
            /**
             * FAST: 5005 - FIX
             * CONS: 5002 <= 12; 5053 <=24;  5054- FIX; 5291 <= 60, 5292 > 60 - VAR
             * --------------------------
             * AUTO: 5352 
             * HOME: 5290 - FIX, 5161 - VAR
             **/                                                    
            p_loan.productId = (loan.loanType === brUtils.enums.loanType.FAST) ?
                                    (loan.aprType === brUtils.enums.aprType.FIX) ? '5005': 'X' :
                               (loan.loanType === brUtils.enums.loanType.CONS) ?
                                    (loan.aprType === brUtils.enums.aprType.FIX) ? 
                                        ((loan.duration <= 12) ? '5002': ((loan.duration <= 24) ? '5053' : '5054')) : 
                                        ((loan.duration <= 60) ? '5291' : '5292') :
                               (loan.loanType === brUtils.enums.loanType.HOME) ? 
                                    (loan.aprType === brUtils.enums.aprType.FIX) ? '5290': '5161' :
                               'X';
            /* s: 5020 - FAST, 5010 - CONS, 5015 - AUTO, 6020 - HOME*/
            p_loan.productId_1 = (loan.loanType === brUtils.enums.loanType.FAST) ? '5020' :
                                 (loan.loanType === brUtils.enums.loanType.CONS) ? '5010' :
                                 (loan.loanType === brUtils.enums.loanType.HOME) ? '6020' : 'X';
            /* @TODO maybe: 6 - 6Months*/
            // p_loan.aprPeriod = '6';                       	
                                             
			
			return $http({
				url: 
                    brUtils.ENV.testMode ? brUtils.test.getResource(provider, loan) +'.html' : //'bankakoper-hitri' +'.html' :
					brUtils.ENV.proxy + 
					provider.apiUrl + product.apiPath
			,	method: 
                    brUtils.ENV.testMode ? "GET" : 
                    "GET"
			,	headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			,   params: {
                    'a': p_loan.amount
                ,   'm': p_loan.duration
                ,   'pid': p_loan.productId               
                ,   'i': p_loan.aprType
                ,   'r': '6'
                ,   'z': p_loan.securityType
                ,   'v': '978'
                ,   's': p_loan.productId_1                
			}
			, 	transformResponse: function(data){
					try {
						var noimg = data.replace(/<img[^>]*>/g,"")  //remove images
                        ,	html = $($.parseHTML(noimg))
                        , 	section = html.find('#ctl00_cplMain_ctl04_pnlFormCalc')
                        , 	section_interest = html.find('#eomctl00_cplMain_ctl04_LoanCalc1')
                        ;	
                        // debugger;                
                        if (section.length == 0) {								
                            throw section.find('div.documentPanel').text();		
                        }			
                        
                        var 
                            apr = brUtils.trans.parseNumber( section_interest.find(".data:contains('Skupna obrestna mera')").text().split('%')[0] )
                        ,   aprMargin = brUtils.trans.parseNumber( section_interest.find(".data:contains('Pribitek')").text().split('%')[1] )
                        ,   aprBase = brUtils.trans.parseNumber( section_interest.find(".data:contains('EURIBOR')").text().split('%')[0] )
                        // amountRepay = amount + costsTotal + interestTotal
                        ,   amount = brUtils.trans.parseNumber( section.find(":contains('Višina kredita'):not(:has('input')) .tabInput").text() )
                        ,   approvalCosts = brUtils.trans.parseNumber( section_interest.find(".data:contains('Stroški odobritve')").text() )
                        ,   securityCosts = brUtils.trans.parseNumber( section_interest.find(".data:contains('Stroški zavarovanja')").text() )
                        ,   interestTotal = brUtils.trans.parseNumber( section.find(":contains('Skupaj obresti')>.tabInput").text() )
                        ,   costsTotal = securityCosts + approvalCosts                        
                        ,   amountRepay = amount + costsTotal + interestTotal
                        ;						
						return {							
						    amount:  amount
						,	annuity: brUtils.trans.parseNumber( section.find(":contains('Mesečna anuiteta'):not(:has('input')) .tabInput").text() )
						,	duration: brUtils.trans.parseNumber( loan.duration )
						,	apr: apr
						, 	ear: brUtils.trans.parseNumber( section.find(":contains('EOM')>.tabInput").text() )
						,	costsTotal: costsTotal
                        ,   interestTotal: interestTotal
						,	amountRepay: amountRepay	
                        ,   aprBase: aprBase                        
                        ,   aprMargin: aprMargin				
						};
					} catch (e) {
						return { code: '5000', error: e };					
					}
				}	
			});
		}
	}
	
})();