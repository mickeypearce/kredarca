(function() {
'use strict';

	angular.module('kredarcaServices').factory('UnicreditApi', UnicreditApi);
	
	UnicreditApi.$inject = ['brUtils', '$http'];
	function UnicreditApi(brUtils, $http) {
		var provider = brUtils.data.getProviders('UNI')[0];
		var service = {
			getLoan: getLoan
		};
		
		return service;
					
		////////////////
        function getLoan(loan) {
            
            var product = brUtils.data.getLoans({providerId: provider.id, type: loan.loanType})[0];
            
            return get(loan, product).then(function(resp){
                resp.data.query = loan;
                resp.data.provider = provider;                    
                resp.data.product = product;
                return resp;
            });
        }
        
		function get(loan, product) { 	
            
            // No queries
            // NO FAST                        
            if (loan.loanType === brUtils.enums.loanType.FAST)
				return brUtils.errors.getErrorAsync({
					code: '1040'
				,	error: 'No querys for loanType=' + loan.loanType
				});

            // MEM
            if (loan.relationType !== brUtils.enums.relationType.MEM)
				return brUtils.errors.getErrorAsync({				
                	code: '1200'
				,	error: 'No querys for relationType=' + loan.relationType
				});

            if (
                // CONS: INS, OTH 
                (loan.loanType === brUtils.enums.loanType.CONS && 
                !_.contains([brUtils.enums.securityType.INS, brUtils.enums.securityType.OTH], loan.securityType)) ||
                // HOME: INS, PRP                    
                (loan.loanType === brUtils.enums.loanType.HOME && 
                !_.contains([brUtils.enums.securityType.INS, brUtils.enums.securityType.PRP], loan.securityType))
            )                                             
                return brUtils.errors.getErrorAsync({
                    code: '1000'
                ,	error: 'No querys for securityType=' + loan.securityType + ' and loanType=' + loan.loanType                    
                });              

            // HOME: 10, 15, 20, 30y
            if (loan.loanType === brUtils.enums.loanType.HOME && !_.contains(['120', '180', '240', '360'], loan.duration))                                                                                      
                return brUtils.errors.getErrorAsync({
                    code: '1100'
                ,	error: 'No querys for duration=' + loan.duration + ' and loanType=' + loan.loanType                    
                });                                           			
            
			
            // Mapping
                       
			var p_loan = angular.copy(loan);                        
            // /* 1 - komitent, 0 - nekomitent*/
			// p_loan.relationType = (loan.relationType === brUtils.enums.relationType.MEM) ? '1' : '0';		
			// /* 94/76 - zavarovanje, 93/48 - porok,  ...  */
			// p_loan.securityType = 
            //     (loan.loanType === brUtils.enums.loanType.CONS) ?
            //         (loan.securityType === brUtils.enums.securityType.INS) ? 
            //             (loan.aprType === brUtils.enums.aprType.FIX) ? '94': '76' : 
            //         (loan.securityType === brUtils.enums.securityType.BFF) ? 
            //             (loan.aprType === brUtils.enums.aprType.FIX) ? '93': '48' :
            //         'X' : 'X';                                                           	                                                                       
            p_loan.productId = product.serviceName;                                                                  
			
			return $http({
				url: 
					brUtils.ENV.testMode ? 'resources/'+ 'UNI-' + loan.loanType +'.json' :
					brUtils.ENV.proxy + 					
                    provider.apiUrl + product.apiPath                    
			,   method: 
					brUtils.ENV.testMode ? "GET" :
					"POST"
			,   headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			, data: $.param({
            /*
            CONS:
            borrow_amount=5000&borrow_months=24&monthly_income=5000&euribor=-0.309&loan_type=type4&loan_currency=EUR&service-name=personal-loan
            HOME:
            price=150000&borrow_amount=100000&monthly_salary=1500&age=30&mortgage_type=type1&amount_currency=EUR&service-name=mortgage&borrow_years=20&financial_indicator=-0.00311
            */
                    'borrow_amount': p_loan.amount
                ,   'borrow_months': p_loan.duration                
                ,   'monthly_income': p_loan.amount               
                ,   'euribor': 0 //-0.309
                ,   'loan_type': 'type3'
                ,   'loan_currency': 'EUR'
                ,   'service-name': p_loan.productId
                // HOME specific
                ,   'price': p_loan.amount * 1.5         
                ,   'monthly_salary': p_loan.amount     
                ,   'age': 20
                ,   'mortgage_type': 'type1'
                ,   'amount_currency': 'EUR'
                ,   'borrow_years': p_loan.duration / 12
                ,   'financial_indicator': 0 //0.00311
			})
			, 	transformResponse: function(data){
					try {
						var json = angular.fromJson(data)
						// Response includes both aprTypes, extracting appropriate
						,	section = _.find(json.results, function(item){
							return ((loan.aprType === brUtils.enums.aprType.VAR) && (item.label === 'Posojilo s spremenljivo obrestno mero')) || 
                                   ((loan.aprType === brUtils.enums.aprType.FIX) && (item.label === 'Posojilo s fiksno obrestno mero'));
						});	
                                        
                        if (angular.isUndefined(section)) {
							throw 'No querys for aprType=' + loan.aprType;		
						};
						
                        // amountRepay = amount + costsTotal + interestTotal
                        var                                   
                            amount = parseFloat(section.loanAmount)                   
                        ,   amountRepay = parseFloat(section.totalPayment)
                        ,   costsTotal = parseFloat(section.approvalFee) + parseFloat(section.riskFee || 0)
                        ,   interestTotal = amountRepay - amount - costsTotal;
                        
						return {						
						    amount: amount
						,	annuity: parseFloat(section.installment)
						,	duration: p_loan.duration //section.tenor * 12
						,	apr: parseFloat(section.interestRate)
						, 	ear: parseFloat(section.dae)
						,	costsTotal: costsTotal
                        ,   interestTotal: interestTotal
						,	amountRepay: amountRepay		
                        ,   aprBase: null
                        ,   aprMargin: null			                        					
						};
					} catch (e) {
						return { code: '5000', error: e };					
					}
				}	
			});
		}
	}
	
})();