(function() {
'use strict';

	angular.module('kredarcaServices').factory('SkbApi', SkbApi);
	
	SkbApi.$inject = ['brUtils', '$http'];
	function SkbApi(brUtils, $http) {
		var provider = brUtils.data.getProviders('SKB')[0];
		var service = {
			getLoan: getLoan
		};
		
		return service;
        
        ////////////////
        function getLoan(loan) {
            
            var product = brUtils.data.getLoans({providerId: provider.id, type: loan.loanType})[0];
            
            return get(loan, product).then(function(resp){
                resp.data.query = loan;
                resp.data.provider = provider;                    
                resp.data.product = product;
                return resp;
            });
        }									
        
        /*
        GET http://www.skb.si/izracuni/loan/fastcashloan (get /izracuni cookie)  + view state        
        GET http://www.skb.si/izracuni/loan/fastcashloan?height=663        
        POST http://www.skb.si/izracuni/loan/fastcashloan;jsessionid=88215BE344D9D047B338087268ABA73C  
        */

		function get(loan, product) { 			
			            
            //1. Get cookie and ViewState parameter (GET)
            //2. Validate cookie (GET)
            //3. Query request (POST)        
            
            var viewstate = null;
            
            // Get Cookie - 1.step           
			// Set-Cookie	JSESSIONID=F39F8009A4DD3F05D749F4E5554E6B88; Path=/izracuni
			// Cookie Path is fixed by proxy to root (/)            
            return $http({
				url: 
                    brUtils.ENV.testMode ? 'resources/'+ 'skb-prefetch' +'.html' : 
                    brUtils.ENV.proxy + 
                    provider.apiUrl + product.apiPath
			,	method: 'GET'
			,	withCredential : true            
            ,	transformResponse: function(data) {
					var noimg = data.replace(/<img[^>]*>/g,"")  //remove images
					,	html = $($.parseHTML(noimg));	
                    
                    // Saving viewstate parameter
                    viewstate = html.find('#javax\\.faces\\.ViewState').attr('value');				
                    return true;
				}       
			}).then(function(){		                
                
                // Validate Cookie - 2.step                                	                	
				return $http({
					url: 
                        brUtils.ENV.testMode ? brUtils.test.getResource(provider, loan) +'.xml' :  
                        brUtils.ENV.proxy + 
                        provider.apiUrl + product.apiPath
				,	method: 'GET'
				,	withCredential : true
                ,	params: { height: '663' }     
				});                                                     
			}).then(function(){                
                                                                
                // We got and set cookies now lets get real
                                                                   
                // No Queries
                                
                // SkbApi doesn't have securityType param, so we have to handle it here                            
                if (
                    // FAST < 24: OUT, BFF - SHORTTERMCLIENT - not available through provider api query 
                    // FAST < 36: INS - CLIENT  
                    (loan.loanType === brUtils.enums.loanType.FAST && 
                    !_.contains([brUtils.enums.securityType.INS], loan.securityType)) || 
                    // CONS: OUT
                    (loan.loanType === brUtils.enums.loanType.CONS && 
                    !_.contains([brUtils.enums.securityType.OUT], loan.securityType)) ||
                    // HOME: PRP, INS, BFF
                    (loan.loanType === brUtils.enums.loanType.HOME && 
                    !_.contains([brUtils.enums.securityType.PRP, brUtils.enums.securityType.INS, brUtils.enums.securityType.BFF], loan.securityType))
                )                                             
                    return brUtils.errors.getErrorAsync({
                        code: '1000'
                    ,	error: 'No querys for securityType=' + loan.securityType + ' and loanType=' + loan.loanType                    
                    });                                    
            
                // Mapping                                
                
                var p_loan = angular.copy(loan);
                /* CASH - gotovina, OTHER - ostali nameni */
                p_loan.purposeLoan = (loan.purposeLoan === brUtils.enums.purposeLoanType.CSH) ? 'CASH' : 'OTHER';
                /* FIXED - fiksna, COMPOSITE - spremenljiva */
                p_loan.aprType = (loan.aprType === brUtils.enums.aprType.FIX) ? 'FIXED' : 'COMPOSITE';
                /* CLIENT - prejema plačo na račun (>6m), NONCLIENT - ne prejema plače na račun, SHORTTERMCLIENT - plača <6m */
                p_loan.relationType = (loan.relationType === brUtils.enums.relationType.MEM) ? 'CLIENT' : 'NONCLIENT';		                
                /* INSURANCE - zavarovalnica, SPONSOR - porok, MORTGAGE - hipoteka */
                p_loan.securityType = (loan.securityType === brUtils.enums.securityType.BFF) ? 'SPONSOR' :
                                    (loan.securityType === brUtils.enums.securityType.INS) ? 'INSURANCE' : 
                                    (loan.securityType === brUtils.enums.securityType.PRP) ? 'MORTGAGE' : 'X';
                // Request body parameters, ex.for fastcashloan
                // ,	data: $.param({
                //     ,	"fastcacshloan:selectcreditconsumption": p_loan.purposeLoan	
                //     ,	"fastcacshloan:duration": p_loan.duration
                //     ,	"fastcacshloan": "fastcacshloan"
                //     ,	"fastcacshloan:submitfastcash":	"fastcacshloan:submitfastcash"
                //     ,	"javax.faces.ViewState": "j_id2" //j_id1, j_id3
                //     ,	"AJAXREQUEST": "_viewRoot"
                //     ,	"fastcacshloan:selectratetype":	p_loan.aprType
                //     ,	"fastcacshloan:selectbusinesscooperation": p_loan.relationType
                //     ,	"fastcacshloan:amount":	p_loan.amount
                //     ,	"fastcacshloan:annuity": ""                  
                // })
                
                var 
                // !!! Fucking typo in provider api: fastcacshloan != fastcashloan, cacshloan != cashloan
                    params_head = product.apiPath.replace('cash', 'cacsh'),
                // Remove 'loan' from apiPath for submit parameter
                    params_submit = product.apiPath.replace('loan', '');  
                    
                var data = {};
                data[params_head + ':selectcreditconsumption'] = p_loan.purposeLoan; // samo pri CONS
                data[params_head + ':duration'] = p_loan.duration;
                data[params_head] = params_head;
                data[params_head + ':submit' + params_submit] = product.apiPath + ":submit" + params_submit;
                data['javax.faces.ViewState'] = viewstate;
                data['AJAXREQUEST'] = '_viewRoot';
                data[params_head + ':selectratetype'] = p_loan.aprType;
                data[params_head + ':selectbusinesscooperation'] = p_loan.relationType;
                data[params_head + ':selectinsurance'] = p_loan.securityType;    // samo pri HOME            
                data[params_head + ':amount'] = p_loan.amount;
                data[params_head + ':annuity'] = '';
                
                return $http({
                    url: 
                        brUtils.ENV.testMode ? brUtils.test.getResource(provider, loan) +'.xml' :
                        brUtils.ENV.proxy + 
                        provider.apiUrl + product.apiPath
                ,	method: brUtils.ENV.testMode ? "GET" : "POST"
                ,	withCredential : true
                ,	headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                ,	data: $.param(data)
                , 	transformResponse: function(data){
                        try {
                            var xml = $($.parseXML(data))
                            ,   section = xml.find("#result")
                            ,   section_interest = xml.find("#resultstatic");
                                            
                            if (section.length == 0) {
                                throw xml.find('.facemessages').text();		
                            }	
                            
                            var                               
                                aprBase = brUtils.trans.parseNumber( section_interest.find( ":contains('Referenčna obrestna mera')" ).text() )
                            ,   aprMargin = brUtils.trans.parseNumber( section_interest.find( ":contains('Obrestni pribitek')" ).text() )
                            ,   apr = brUtils.trans.parseNumber( section_interest.find( ":contains('Skupna obrestna mera')" ).text() )
                            // amountRepay = amount + costsTotal + interestTotal
                            ,	amount = brUtils.trans.parseNumber( section.find( ":contains('Znesek kredita')" ).text() )
                            // Split - Must locate number, as ',' is in text and parseNumber converts all ',' to '.'
                            ,   amountRepay = brUtils.trans.parseNumber( section.find( ":contains('Skupni znesek, ki ga mora plačati potrošnik')" ).text().split(':')[1].trim() )
                            ,   approvalCosts = brUtils.trans.parseNumber( section.find( ":contains('Stroški odobritve kredita')" ).text() )
                            ,   securityCosts = brUtils.trans.parseNumber( section.find( ":contains('Stroški zavarovanja kredita')" ).text() )
                            // Provider includes loan processing and account monthly fee in ear calculation!
                            ,   processingMonthlyCosts = brUtils.trans.parseNumber( section.find( ":contains('Mesečni stroški vodenja kredita')" ).text() )
                            ,   accountMonthlyCosts = brUtils.trans.parseNumber( section.find( ":contains('Mesečni stroški vodenja osebnega računa')" ).text() )
                            ,   costsTotal = securityCosts + approvalCosts + (processingMonthlyCosts*p_loan.duration) + (accountMonthlyCosts*p_loan.duration)                         
                            ,   interestTotal = amountRepay - amount - costsTotal;
                                                                                           		
                            					
                            return {	
                                amount: amount
                            ,	annuity: brUtils.trans.parseNumber( section.find( ":contains('Mesečna anuiteta')" ).text() )
                            ,	duration: brUtils.trans.parseNumber( section.find( ":contains('Doba odplačila')" ).text())
                            ,	apr: apr
                            , 	ear: brUtils.trans.parseNumber( section.find( ":contains('Efektivna obrestna mera')" ).text() )
                            ,	costsTotal: costsTotal
                            ,   interestTotal: interestTotal
                            ,	amountRepay: amountRepay		
                            ,   aprBase: aprBase                        
                            ,   aprMargin: aprMargin				
                            };
                        } catch (e) {                            
                            return { code: '5000', error: e };					
                        }
                    }                    	                    
                });
                
            });
		}
	}
	
})();