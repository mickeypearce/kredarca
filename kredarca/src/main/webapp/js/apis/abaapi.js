(function() {
'use strict';

    angular.module('kredarcaServices').factory('AbankaApi', AbankaApi);
	
	AbankaApi.$inject = ['brUtils', '$http'];	
	function AbankaApi(brUtils, $http) {
        var provider = brUtils.data.getProviders('ABA')[0];
		var service = {
			getLoan: getLoan
		};
		
		return service;
					
		////////////////
        function getLoan(loan) {
            
            var product = brUtils.data.getLoans({providerId: provider.id, type: loan.loanType})[0];
            
            return get(loan, product).then(function(resp){
                resp.data.query = loan;
                resp.data.provider = provider;                    
                resp.data.product = product;
                return resp;
            });
        }
        
		function get(loan, product) { 			
            
            // No queries
            
            if (loan.loanType === brUtils.enums.loanType.FAST)
				return brUtils.errors.getErrorAsync({
					code: '1040'
				,	error: 'No querys for loanType=' + loan.loanType
				});	 
            
            if (
                // INS, BFF, PRP                   
                !_.contains([
                    brUtils.enums.securityType.INS, brUtils.enums.securityType.BFF, brUtils.enums.securityType.PRP
                ], loan.securityType)
            )                                             
                return brUtils.errors.getErrorAsync({
                    code: '1000'
                ,	error: 'No querys for securityType=' + loan.securityType + ' and loanType=' + loan.loanType                    
                });
            
			// Mapping
            
			var p_loan = angular.copy(loan);
			/* 10 - zavarovalnica, 13 - hipoteka, 11 - poroki */
			p_loan.securityType = (loan.securityType === brUtils.enums.securityType.INS) ? '10' : 
								(loan.securityType === brUtils.enums.securityType.PRP) ? '13' : 
								(loan.securityType === brUtils.enums.securityType.BFF) ? '11' : 'X';
			/* 0 - denar, 1 - stanovanje, 2 - avto*/                   
            p_loan.productId = product.apiPath;  
			/* true - komitent, false - nekomitent*/
			p_loan.relationType = (loan.relationType === brUtils.enums.relationType.MEM) ? 'true' : 'false';
                        
			
			return $http({		   
				url: 
					brUtils.ENV.testMode ? 'resources/'+ 'ABA-CONS' +'.json' :
					brUtils.ENV.proxy + 					
                    provider.apiUrl
				, method: 
					brUtils.ENV.testMode ? "GET" :
					"POST"
				, headers: {
					'Content-Type': 'application/json; charset=UTF-8'
				}
				, params: {} 
				, data: { 
					doba: p_loan.duration
				,	komitentnost: p_loan.relationType
				,	namenId: p_loan.productId
				// Only for INS, otherwise must be false - yeah stupid as hell!
				,	placaNaTRR: (loan.securityType === brUtils.enums.securityType.INS) ? 'true': false
				,	vrstaIzracunaId: '0'
				,	zavarovanjeId: p_loan.securityType
                ,   znesekKredita: p_loan.amount
				}				
				, transformResponse: function(data){
					try {
						var json = angular.fromJson(data)
						// Response includes both aprTypes, extracting appropriate
						,	section = _.find(json[0].calculations, function(item){
							return ((loan.aprType === brUtils.enums.aprType.VAR) && (item.interestRateLegalTextKey === 'variableInterestRate')) || ((loan.aprType === brUtils.enums.aprType.FIX) && (item.interestRateLegalTextKey === 'fixedInterestRate'));
						});
						
						if (angular.isUndefined(section)) {
							throw 'No querys for aprType=' + loan.aprType;		
						};
						
                        var                                                          
                            aprBase = brUtils.trans.parseNumber( (loan.aprType === brUtils.enums.aprType.VAR) ? section.rom.split('=')[1] : 0 )
                        ,   aprMargin = brUtils.trans.parseNumber( (loan.aprType === brUtils.enums.aprType.VAR) ? section.interestRate.split('+')[1] : section.interestRate )      
                        ,   apr = aprBase + aprMargin               
                        // amountRepay = amount + costsTotal + interestTotal
                        ,	amount = brUtils.trans.parseNumber( section.creditAmount )                        
                        ,   amountRepay = brUtils.trans.parseNumber( section.totalAmountToPay )
                        ,   approvalCosts = brUtils.trans.parseNumber( section.approvalFees )
                        ,   securityCosts = brUtils.trans.parseNumber( section.insuranceCos )                        
                        ,   costsTotal = securityCosts + approvalCosts                         
                        ,   interestTotal = amountRepay - amount - costsTotal;
                        
						return {						
						    amount: amount
                        ,	annuity: brUtils.trans.parseNumber( section.monthlyPayment )
						,	duration: section.repaymentPeriod
						,	apr: apr
                        , 	ear: brUtils.trans.parseNumber( section.effectiveInterestRate )
						,	costsTotal: costsTotal
                        ,   interestTotal: interestTotal
						,	amountRepay: amountRepay		
                        ,   aprBase: aprBase
                        ,   aprMargin: aprMargin		                        					
						};
					} catch (e) {
						return { code: '5000', error: e };					
					}
				}					
			});	
								
		};
			
	};

})();