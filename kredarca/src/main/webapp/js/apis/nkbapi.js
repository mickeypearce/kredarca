(function() {
'use strict';

	angular.module('kredarcaServices').factory('NkbmApi', NkbmApi);
	
	NkbmApi.$inject = ['brUtils', '$http'];
	function NkbmApi(brUtils, $http) {
		var provider = brUtils.data.getProviders('NKB')[0];
		var service = {
			getLoan: getLoan
		};
		
		return service;
					
		////////////////
        function getLoan(loan) {
            
            var product = brUtils.data.getLoans({providerId: provider.id, type: loan.loanType})[0];
            
            return get(loan, product).then(function(resp){
                resp.data.query = loan;
                resp.data.provider = provider;                    
                resp.data.product = product;
                return resp;
            });
        }
        
		function get(loan, product) { 			
			
            // No queries
              
            if (
                // INS, BFF, PRP, OTH                     
                !_.contains([
                    brUtils.enums.securityType.INS, brUtils.enums.securityType.BFF, brUtils.enums.securityType.PRP, brUtils.enums.securityType.OTH
                ], loan.securityType)
            )                                             
                return brUtils.errors.getErrorAsync({
                    code: '1000'
                ,	error: 'No querys for securityType=' + loan.securityType + ' and loanType=' + loan.loanType                    
                });
                
            if (
                // FAST: FIX                   
                (loan.loanType === brUtils.enums.loanType.FAST && 
                !(loan.aprType === brUtils.enums.aprType.FIX)) || 
                // CONS: <= 12: FIX; 12-<=60m: FIX, VAR; >60m: VAR
                (loan.loanType === brUtils.enums.loanType.CONS && 
                !(loan.aprType === brUtils.enums.aprType.FIX && loan.duration <= 60) && 
                !(loan.aprType === brUtils.enums.aprType.VAR && loan.duration > 12))
            )                                             
                return brUtils.errors.getErrorAsync({
                    code: '1100'
                ,	error: 'No querys for aprType=' + loan.aprType + ' and loanType=' + loan.loanType                    
                });
            
			// Mapping
            
			var p_loan = angular.copy(loan);            
            /* 1 - fiksna, 8 - spremenljiva*/
			p_loan.aprType = (loan.aprType === brUtils.enums.aprType.FIX) ? '1' : '8';
            /* 6 - komitent, 7 - nekomitent*/
			p_loan.relationType = (loan.relationType === brUtils.enums.relationType.MEM) ? '6' : '7';		
			/* 1 - Stroški odplačevanja, 2 - porok, 3 - hipoteka, 4 - prvovrstna zavarovanja */
			p_loan.securityType = (loan.securityType === brUtils.enums.securityType.INS) ? '1' : 
								(loan.securityType === brUtils.enums.securityType.BFF) ? '2' : 
                                (loan.securityType === brUtils.enums.securityType.PRP) ? '3' : 								
                                (loan.securityType === brUtils.enums.securityType.OTH) ? '4' : 'X';	               
            /* This api has separate products for short and long term loans; short term for loan.duration <= 12m, long term > 12m                
               Stored as an apiPath in loans data - a stretch! */
            p_loan.productId = //brUtils.ENV.testMode ? '12' :
                               (loan.duration <= 12) ? product.apiPath.split(',')[0] : product.apiPath.split(',')[1]; 
                                                                              
			
			return $http({
				url: 
                    brUtils.ENV.testMode ? brUtils.test.getResource(provider, loan) +'.html':
					brUtils.ENV.proxy + 
					provider.apiUrl
			,	method: 
                    brUtils.ENV.testMode ? "GET" : 
                    "GET"
			,	headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			,   params: {
                    productid: p_loan.productId
                ,   validate: '1'
                ,   producttypeid: p_loan.relationType
                ,   productinterestratetypeid: p_loan.aprType
                ,   insurancetypeid: p_loan.securityType
                ,   productadditionalcostdimension1jointlistid: '0'
                ,   productadditionalcostdimension2jointlistid: '0'
                ,   productadditionalcostdimension3jointlistid: '0'
                ,   amount: p_loan.amount
                ,   annuity: ''
                ,   term: p_loan.duration
                ,   termtypeid: '2' // 2- Months, 3- Year
                ,   currencyid: '978'
                ,   annuitycurrencyid: '978'
                ,   simulation: '0'
                ,   simulationinterestratepercent: '0'
                ,   simulationpaymentcostpercent: '0'
                ,   productadditionalcostversionid: '274' //'275', '276' ? '272'-HOME
                ,   productinterestrateversionid: ''
                ,   _: '1452441009113' //'1458655581084'  incrementing?
			}
			, 	transformResponse: function(data){
					try {
						var noimg = data.replace(/<img[^>]*>/g,"")  //remove images
                        ,	html = $($.parseHTML(noimg))
                        , 	section = html //.find('#eventlistdataproduct' + p_loan.productId)
                        ,   section_interest = html.find('div.ajaxproductcalculatorcomment').text().split('%');                        	
                                        
                        if (section.find('div.ajaxproductcalculatortitle').length == 0) {								
                            throw section.find('div.errortext.errortextpadding').text();		
                        }		
                        
                        var                                                          
                            apr = brUtils.trans.parseNumber( section_interest[0] )
                        ,   aprBase = brUtils.trans.parseNumber( section_interest[1].split('Euribor')[1] )
                        ,   aprMargin = brUtils.trans.parseNumber( section_interest[2] )                        
                        // amountRepay = amount + costsTotal + interestTotal
                        ,	amount = brUtils.trans.parseNumber( section.find( ":contains('Skupni znesek kredita').ajaxproductcalculatorleft").next().text() )                        
                        ,   amountRepay = brUtils.trans.parseNumber( section.find(":contains('Skupni znesek za plačilo').ajaxproductcalculatorleft").next().text() )
                        ,   approvalCosts = brUtils.trans.parseNumber( section.find( ":contains('Stroški odobritve').ajaxproductcalculatorleft").next().text() )
                        ,   securityCosts = brUtils.trans.parseNumber( section.find( ":contains('Stroški odplačevanja').ajaxproductcalculatorleft").next().text() )                        
                        ,   costsTotal = securityCosts + approvalCosts                         
                        ,   interestTotal = amountRepay - amount - costsTotal;
                            							
						return {													
                            amount: amount
						,	annuity: brUtils.trans.parseNumber( html.find( ":contains('Mesečna obveznost').ajaxproductcalculatorleft").next().text() )
						,	duration: brUtils.trans.parseNumber( html.find( ":contains('Število obrokov').ajaxproductcalculatorleft").next().text() )
						,	apr: apr 
						, 	ear: brUtils.trans.parseNumber( html.find( ":contains('Efektivna obrestna mera').ajaxproductcalculatorleft").next().text() )
						,	costsTotal: costsTotal
                        ,   interestTotal: interestTotal
						,	amountRepay: amountRepay 
                        ,   aprBase: aprBase                        
                        ,   aprMargin: aprMargin					
						};
					} catch (e) {
						return { code: '5000', error: e };					
					}
				}	
			});
		}
	}
	
})();