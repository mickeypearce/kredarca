(function() {
'use strict';

	angular.module('kredarcaServices').factory('LonApi', LonApi);
	
	LonApi.$inject = ['brUtils', '$http'];
	function LonApi(brUtils, $http) {
        var provider = brUtils.data.getProviders('LON')[0];		
        var service = {
			getLoan: getLoan
		};
		
		return service;
		
		function getLoan(loan){
            var product = brUtils.data.getLoans({providerId: provider.id, type: loan.loanType})[0];
            
			// Mapping
			var p_loan = angular.copy(loan);
			/* premija - zavarovalnica*/
			// p_loan.securityType = (loan.securityType === brUtils.enums.securityType.INS) ? 'premija' : 'premija';
			if (loan.securityType === brUtils.enums.securityType.INS)
				p_loan.securityType = 'premija'
			else
				return brUtils.errors.getErrorAsync({
					code: provider.id + '-9000'
				,	error: 'No querys for securityType=' + loan.securityType + ', loan=' + angular.toJson(loan)
				});	
				
			/* premija - zavarovalnica*/
			p_loan.relationType = (loan.relationType === brUtils.enums.relationType.MEM) ? 'komitent' : 'novkomitent';
			
			return $http({		   
				url: 
					brUtils.ENV.testMode ? 'resources/'+ 'lon-potrosnik' +'.html' :
					brUtils.ENV.proxy + 
					"http://www.lon.si/LON_SI.asp"
				, method: 
					brUtils.ENV.testMode ? "GET" :
					"GET"
				, headers: {}
				// znesek=2000&st_mesecev=36&zavarovanje=premija&komitent=komitent&knof=Izra%E8un&lytoo=pripomocki%2Finformativni_izracuni%2Fpotrosniski_akcija
				, params: { 
					znesek: p_loan.amount 
				,	st_mesecev: p_loan.duration
				,	zavarovanje: p_loan.securityType
				,	komitent:	p_loan.relationType
				,	knof: 'Izračun'
				,	lytoo: 'pripomocki/informativni_izracuni/potrosniski_akcija'
				}		
				, data: {} 
				, transformResponse: function(data){
					try {
						var noimg = data.replace(/<img[^>]*>/g,"")  //remove images
						,	html = $($.parseHTML(noimg))
						,	section = html.find('#MAIN table:nth-child(4) tr:nth-child(2) td:nth-child(2)');	
										
						if (section.children().length == 0) {
							throw section.text();		
						};		
									
						return {
							query: loan                        
						,	provider: provider	
                        ,	product: product
						,	amount: brUtils.trans.parseNumber( section.find('tr:nth-child(2) td:nth-child(2)').text().split('€')[0].trim() )
						,	annuity: brUtils.trans.parseNumber( section.find('tr:nth-child(3) td:nth-child(2)').text().split('€')[0].trim() )
						,	duration: section.find('tr:nth-child(4) td:nth-child(2)').text().trim()
						,	apr: brUtils.trans.parseNumber( section.find('tr:nth-child(10) td:nth-child(2)').text().split('%')[0].trim() )
						, 	ear: brUtils.trans.parseNumber( section.find('tr:nth-child(11) td:nth-child(2)').text().split('%')[0].trim() )
						,	costsTotal: brUtils.trans.parseNumber( section.find('tr:nth-child(3) td:nth-child(2)').text().split('€')[0].trim() )
						,	amountRepay: brUtils.trans.parseNumber( section.find('tr:nth-child(13) td:nth-child(2)').text().split('€')[0].trim() )
						};
					} catch (e) {
						return { code: provider.id + '-9999', message: data, error: e };					
					}
				}					
			});	 
			
		}
	}

})();